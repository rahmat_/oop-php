<?php
    require_once('Animal.php');
    
    class Ape extends Animal{
        public $legs = 2;
        public $cold_blooded = "no";

        // funtion dengan parameter
        public function yell($suara){
            return "Yell : $suara ";
        }
    }
