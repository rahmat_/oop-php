<?php
    require_once('Animal.php');
    
    class Frog extends Animal{
        public $legs = 4;
        public $cold_blooded = "no";

        // funtion tanpa parameter
        public function jump(){
            return "Hop Hop";
        }
    }

?>